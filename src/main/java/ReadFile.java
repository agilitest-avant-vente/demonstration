import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;
import java.io.*;

public class ReadFile extends ActionTestScript {

	@Test
	public void testMain() {
		final String filePath = getParameter(0).toString();
		final File file = new File(filePath);

		final BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			final String value = reader.readLine();
			reader.close();
			returnValues(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}