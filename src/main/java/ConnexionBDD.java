import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;
import java.sql.*;

public class ConnexionBDD extends ActionTestScript{

	/**
	 * Test Name : <b>ConnexionBDD</b>
	 * Test Author : <b>LAPTOP-206PEVQR\maxim</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){
		// -----------------------------------------------
		// Get parameters passed by the calling script :
		// getParameter(int index)
		// -----------------------------------------------
		// String param0 = getParameter(0).toString();
		// int param0 = getParameter(0).toInt();
		// double param0 = getParameter(0).toDouble();
		// boolean param0 = getParameter(0).toBoolean();
		// -----------------------------------------------
		// int it = getIteration(); -> return current iteration loop
		// String path = getCsvFilePath(); -> return csv file path sent as parameter to call current script
		// File file = getCsvFile(); -> return csv file sent as parameter to call current script
		// File file = getAssetsFile("[relative path string]"); -> return a file in the project's 'assets' folder
		// String url = getAssetsUrl("[relative path string]"); -> return url path of a file in the project's 'assets' folder

		String data = "";		
		
		try
			{
			//étape 1: charger la classe de driver
			Class.forName("com.mysql.jdbc.Driver");
			  
			//étape 2: connexion à la base bdd_agilitest situé sur le localhost port 3306, user root mdp vide
			Connection connBdd = DriverManager.getConnection("jdbc:mysql://localhost:3306/bdd_agilitest", "root", "");
			  
			//étape 3: exécution de la requête
			Statement stmt = connBdd.createStatement();
			ResultSet res = stmt.executeQuery("SELECT * FROM team");
			
			//étape 4: traitement du résultat
			//INSERT INTO `team` (`Prenom`, `Role`) VALUES ('XXX', 'R&D');
			//SELECT * FROM `team` WHERE 1;
			while (res.next()) {
				//System.out.println(res.getString("objet"));
				String name = res.getString("Prenom");
				String role	= res.getString("Role");
				data = data + " / " + name +" : " + role;
			}
			
			//étape 5: fermez l'objet de connexion
			connBdd.close();
			returnValues ("Le resultat de la requete est : "+data);
			}
			
			catch(Exception e){ 
			//On capte l'erreur pour la retourner si besoin
			System.out.println(e);
			returnValues (e.toString());
			}
				
		
		// -----------------------------------------------
		// Return string values to calling script :
		// returnValues(String ...)
		// -----------------------------------------------
		// returnValues("value", stringVariable);
	}
}