# ------------------------------------------------------------------------------
# Get parameter passed by the calling script
# ------------------------------------------------------------------------------
# ex. param0 = ats_parameter(0)
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Send console output logs
# ------------------------------------------------------------------------------
# ex. ats_log("Ats output log ...")
# ------------------------------------------------------------------------------

# This is an example of Python script, you can delete following lines ...

from time import sleep
import sys
from datetime import datetime


def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)
	sleep(5)
        
        # Obtenir la date et l'heure actuelles
	now = datetime.now()
	#Nom de l'utilisateur passé en param0
	nom = ats_parameter(0)
	
        # Formater la date et l'heure
	current_date = now.strftime("%d/%m/%Y")  # Format: jour/mois/année
	current_time = now.strftime("%H:%M:%S")  # Format: heure:minute:seconde
	
        # Afficher les résultats
	print(f"\nDate du jour : {current_date}")
	print(f"\nHeure actuelle : {current_time}")
	
        # Fin du script
	print(f"\nMerci d'avoir utilisé ce programme. À bientôt {nom}")
	ats_log("Merci d'avoir utilisé ce programme. À bientôt "+nom)
	ats_return ([current_date,current_time])
    
main()

# ------------------------------------------------------------------------------
# Return values to the calling script
# ------------------------------------------------------------------------------
# ex. ats_return ('resultvalue')
# or ats_return (['resultvalue1','resultvalue2', ...])
# ------------------------------------------------------------------------------