// ------------------------------------------------------------------------------
// Get parameter passed by the calling script
// ------------------------------------------------------------------------------
// ex. var param0 = ats_parameter(0);
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Use 'ats_element' to get the web element passed by the calling script
// ------------------------------------------------------------------------------
// ex. ats_element.style.visibility = 'hidden';
// ------------------------------------------------------------------------------

console.log('Javascript code launched from ATS execution ...');

var param0 = ats_parameter(0);

// Obtenir la date et l'heure actuelles
const now = new Date();

// Formater la date au format "jour/mois/année"
const current_date = now.toLocaleDateString("fr-FR"); // Exemple : "13/11/2024"

// Formater l'heure au format "heure:minute:seconde"
const current_time = now.toLocaleTimeString("fr-FR"); // Exemple : "15:30:45"

// Afficher les résultats
console.log("Date actuelle :" + current_date);
console.log("Heure actuelle :" + current_time);
console.log("Merci d'avoir utilisé ce programme. À bientôt "+param0);
ats_return ([current_date,current_time])

// ------------------------------------------------------------------------------
// Return values to the calling script
// ------------------------------------------------------------------------------
// ex. ats_return ('resultvalue');
// or ats_return (['resultvalue1','resultvalue2', ...]);
// ------------------------------------------------------------------------------